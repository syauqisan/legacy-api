interface Contact{
    contact_id_type: string;
    contact_id: string;
    deleted; boolean;
}

export interface SaveContactsParam{
    contacts: Array<Contact>;
    device_id: string;
    VERSION_CODE: string;
}

/**
 * in.co.eko.fundu.models.ContactResponseItem
 */
interface ContactResponseItem{
    dummy_customer:boolean;
    contact_id_type: string;
    contact_id: string;
    contact: string;
    deleted: boolean;
    ii: string;
}

export interface SaveContactsResponse{
    contact_response_list: Array<ContactResponseItem>;
}
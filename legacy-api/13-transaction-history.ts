interface HistoryUser {
    id: string;
    location: Array<number>;
    rating: number;
}
/**
 * class in.co.eko.fundu.models.TransactionHistoryModel
 */
export interface TransactionHistoryModel {
    tx_history_id: string;
    tx_id: string;
    tx_status: string;
    tx_amount: number;
    country_shortname: string;
    created_at: string;
    created_by: string;
    customer_name: string;
    customer_mobile: string;
    tx_type: string;
    role: string;
    user_image: string;
    seeker: HistoryUser;
    provider:HistoryUser;
    seeker_charge: number;
    provider_charge: number;

}
export interface TransactionHistoryResponse {
    status: string;
    message?: string;
    historyData: Array<TransactionHistoryModel> | null;
}
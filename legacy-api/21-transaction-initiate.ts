interface TransactionSender{
    alias: string;
    id_type: string;
    id: string;
}

interface TransactionRecepient{
    alias: string;
    id_type: string;
    id: string;
    recipient_id: string;
}

export interface TransactionInitiateParam{
    currency: string;
    fee: string;
    provider_charge: string;
    amount: string;
    hold_timeout: string;
    request_id: string;
    sender: TransactionSender;
    recipient: TransactionRecepient;
}

interface TransactionDataDetail{
    tid: string;
}

interface TransactionData{
    data: TransactionDataDetail;
}

export interface TransactionInitiateResponse{
    data: TransactionData;
}
export interface CheckIsMerchantResponse {
    status: 'ERROR' | 'Success';
    /**
     * its abit make client difficult 
     */
    message?: string;
}
export interface PinRequestParam {
    /**
     * sort of uuid
     */
    custid: string;
    /**
     * pin to save
     */
    fundu_pin : string;
    
    country_shortname: string;
}
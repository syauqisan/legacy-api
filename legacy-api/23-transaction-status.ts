export interface TransactionStatusResponse{
    transaction_id: string;
    request_id: string;
    transaction_state: number;
}
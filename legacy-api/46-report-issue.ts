export interface ReportIssue{
    summary: string;
    description: string;
    country_shortname: string;
}
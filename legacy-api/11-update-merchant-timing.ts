export interface UpdateMerchantTimingParam {
    days: string;
    opening_time: string;
    closing_time: string;
    country_shortname: string;
    contact_id_type: string;
    contact_id: string; 
}

interface UpdateMerchantTimingResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
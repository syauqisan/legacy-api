export interface TransactionParam{

    transaction_id: string;
    amount: string;
    contact_id: string;
    recipient_id: string;
    country_shortname: string;
    status: string;
}

interface TransactionDataDetail{
    tid: string;
}

interface TransactionData{
    data: TransactionDataDetail;
}

export interface TransactionResponse{
    data: TransactionData;
}
export interface ConfirmationRequestParam {
    /**
     * look like id of constomer
     */
    custid: string;
    /**
     * probably amout of transaction
     */    
    amount: number;
    /**
     * short name of country
     */
    country_shortname: string;
    type: string;
}

/**
 * 
 */
export interface ConfirmationRequestResponse {
    /**
     * ERROR or else = success
     */
    status: string;
    message: string;
}

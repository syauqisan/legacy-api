export interface ResetPINParam {
    custid: string;
    country_shortname: string;
    new_fundu_pin: string;
    role: string;
}


export interface  ResetPINResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
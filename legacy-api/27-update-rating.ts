export interface UpdateRatingParam{
    rated_by_id: string;
    transaction_id: string;
    rating: number;
    comments: string;
}

export interface UpdateRatingResponse {
    average_rating
}
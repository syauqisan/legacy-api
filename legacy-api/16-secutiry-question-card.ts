interface SecurityQuestion{
    question_id: string;
    question_value: string;
    answer_mode: string;
    answer_check: string;
    answer_min_length: number;
    answer_max_length: number;
    place_holder: string;
}

export interface SecurityQuestionCardResponse{
    status: string;
    data: SecurityQuestion;
}
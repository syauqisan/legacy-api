interface CampaignAPI{
    campaign_title: string;
    campaign_link: string;
    icon_link: string;
}
export interface CampaignApiResponse{
    campaign: Array<CampaignAPI>
}
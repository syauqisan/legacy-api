export interface PushMessageToMerchantParam{
    name: string;
    country_shortname: string;
    mobile: string;
}


export interface UpdateAndPostSettingParam{
    auto_cash_out: boolean;
    notification: boolean;
    share_location: boolean;
}

interface InnerObject{
    verified: boolean;
    custid: string;
    allow_withdraw: string;
    contact_type: string;
    merchant_img_url: string;
    opening_time: string;
    business_name: string;
}

export interface UpdateAndPostSettingResponse{
    message: string;
    data: InnerObject;
}
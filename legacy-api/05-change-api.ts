export interface ChangePinParam {
    custid: string;
    country_shortname: string;
    old_fundu_pin: string;
    new_fundu_pin: string;
    role: string;
}

export interface ChangePinResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
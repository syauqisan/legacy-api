export interface KenTransportApiParam{
    provider_custid: string;
    amount: string;
    seeker_custid: string;
    country_shortname: string;
    totp: string;
    fundu_pin: string;
    type: string;
}

export interface KenTransportApiResponse{
    status: string;
    message: string;
}
interface TransactionStatus{
    pgMeTrnRefNo: string;
    status: string;
    payerVA: string;
    statusDesc: string;
    txnAmount: string;
    npciTxnId: string;
    payerAccountNo: string;
    tranAuthdate: string;
    additional: string;
    refId: string;
    payerIfsc: string;
    payerAccName: string;
    approvalCode: string;
    orderNo: string;
    responsecode: string;
    address: string;
}

export interface TransactionConfirmParam{
    transaction_id: string;
    request_id: string;
    status: TransactionStatus;
}

interface TransactionConfirmMessage{
    code: string;
}

export interface TransactionConfirmResponse{
    message: TransactionConfirmMessage;
}
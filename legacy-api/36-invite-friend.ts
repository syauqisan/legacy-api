/**
 * in.co.eko.fundu.models.Contact.Identities
 */
interface Identities{
    additionalIdType: string;
    additionalIdValue: string;
    deviceToken: string;
    recipientId: string;
}

export interface InviteFriendParam{
    identities: Array<Identities>
}
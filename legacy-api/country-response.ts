
export interface CountryResponse {
    country_name: string;
    start_with: string;
    country_code: string;
    /**
     * max length for phone number
     * if max length is exceed, keyboard will be hidden
     */
    number_length: number;
    /**
     * currency symbol
     */
    symbol: string;
    enable: boolean;
    country_shortname: string;
}
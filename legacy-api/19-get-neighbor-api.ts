/**
 * in.co.eko.fundu.models.Neighbour
 */
interface Neighbor{
    distance: number;
    id: string;
}

export interface GetNeighborsApiResponse{
    contacts: Array<Neighbor>;
}
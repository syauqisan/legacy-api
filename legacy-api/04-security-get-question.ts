export interface GetSecurityQuestionResponse {
    question_id: string; 
    question_value: string; 
    answer_mode: string; 
    answer_check: string; 
    answer_min_length: string;
    answer_max_length: string;
    place_holder: string;
}
interface CheckAnserResetPINParam {
    custid: string;
    question_id: string;
    card_number: string;
    /**
     * month + year
     */
    card_expiry: string;
    answer: string
}

export interface  CheckAnserResetPINResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
export interface FinalRegistrationApiParam{
    custid: string;
    mobile: string;
    country_shortname: string;
    question_id: string;
    answer: string;
    accno: string;
    card_number: string;
    card_expiry: string;
    cvv: string;
    email: string;
    fundu_pin: string;
    contact_type: string;
    bank_name: string;
}

interface FinalRegistration{
    verified:boolean;
    custid: string;
    allow_withdraw: string;
    contact_type: string;
    merchant_img_url: string;
    opening_time: string;
    business_name: string;
}

export interface FinalRegistrationApiResponse{
    message: string;
    data: FinalRegistration;
}
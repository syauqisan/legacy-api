/**
 * Not 
 */
interface Coordinate{
    coordinates: Array<number>;
}

export interface VerifyTranxCode{

    transaction_id: string;
    request_id: string;
    code: string;
    otp_exchange_location: Coordinate;
}
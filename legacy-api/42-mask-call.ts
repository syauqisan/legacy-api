export interface MaskCallParam{
    transaction_id: string;
}

export interface MaskCallResponse{
    message: string;
}
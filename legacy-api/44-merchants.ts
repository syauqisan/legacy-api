interface Address{
    line1: string;
    line2: string;
    city: string;
    state: string;
    zip: string;
}

export interface MerchantResponse{
    customer_id: string;
    mobile: string;
    name: string;
    address: Array<Address>;
}
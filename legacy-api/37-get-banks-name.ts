interface BanksNameData{
    accno: string;
    ifsc: string;
}

/**
 * in.co.eko.fundu.models.LinkAccountItem
 */
interface LinkAccountItem{
    sb_status: string;
    sb_message: string;
    additionalIdentity: string;
    additional_id_type: string;
    additional_id_value: string;
    updated_by: string;
    name: string;
    visible: boolean;
    fundu_db_status: string;
    fundu_db_message: string;
    recipient_id: number;
}

export interface GetBanksNameResponse{
    data: BanksNameData;
    identitiesResponse: Array<LinkAccountItem>;
}
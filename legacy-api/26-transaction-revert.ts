export interface TransactionRevertParam{
    transaction_id: string;
    contact_id: string;
    recipient_id: string;
    country_shortname: string;
    request_id: string;
}
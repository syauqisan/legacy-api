/**
 * In the code this goes with variable name IdentitiesArray,
 * but the value were just one element.
 * So it changed to non array and strip "array" from the var name.
 */
interface Identities{
    name: string;
    visible: boolean;
    additional_id_type: string;
    additional_id_value: string;
}

interface AdditionalEntitiesObject{
    identities: Identities;
}

export interface CustomerParam{
    additional_identities: AdditionalEntitiesObject;
    country_shortname: string;
    mobile: string;
    accno: string;
    ifsc: string;
    vpa: string;
    recipient_id: number;
}
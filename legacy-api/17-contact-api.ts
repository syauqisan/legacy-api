/**
 * in.co.eko.fundu.models.Contact.Location
 */
interface Location{
    coordinates: Array<number>;
}
/**
 * in.co.eko.fundu.models.Contact.Identities
 */
interface Identities{
    additionalIdType: string;
    additionalIdValue: string;
    deviceToken: string;
    recipientId: string;
}

/**
 * in.co.eko.fundu.models.Contact.AdditionalIdentities
 */
interface AdditionalIdentities{
    identities: Array<Identities>;
}

export interface ContactApiParam{
    rating: string;
    name: string;
    mobile: string;
    countryCode: string;
    postalCode: string;
    locationShortCode: string;
    placeName: string;
    contactIdType: string;
    contactId: string;
    contactType: string;
    deviceId: string;
    deviceType: string;
    deviceToken: string;
    parseInstallationId: string;
    postalAddress: string;
    gcmSenderId: string;
    additionalIdentities: AdditionalIdentities;
    incorp_businessNo: string;
    vertical_market: string;
    business_name: string;
    business_type: string;
    physical_location: string;
    merchant_img_url: string;
    days: string;
    opening_time: string;
    closing_time: string;
    person_img_url: string;
    country_shortname: string;
    sim_number: string;
    imei_number: string;
}

/**
 * in.co.eko.fundu.models.Contact
 */
export interface ContactApiResponse{
    rating: string;
    autoCashOut: boolean;
    name: string;
    mobile: string;
    countryCode: string;
    postalCode: string;
    locationShortCode: string;
    placeName: string;
    contactIdType: string;
    contactId: string;
    contactType: string;
    deviceId: string;
    deviceType: string;
    deviceToken: string;
    parseInstallationId: string;
    postalAddress: string;
    verified: boolean;
    deleted: boolean;
    gcmSenderId: string;
    location: Location;
    additionalIdentities: AdditionalIdentities;
    country_shortname: string;
    incorp_businessNo: string;
    vertical_market: string;
    business_name: string;
    business_type: string;
    physical_location: string;
    merchant_img_url: string;
    days: string;
    opening_time: string;
    closing_time: string;
}
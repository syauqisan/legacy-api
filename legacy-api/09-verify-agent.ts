export interface VerifyMerchantAgentParam {
    mobile: string;
    business_name: string;
    incorp_businessNo: string;
    business_type: string;
    vertical_market: string;
    contact_person_name: string;
    contact_person_phone:string;
    allow_withdraw: string;
    opening_time: string;
    closing_time: string;
    days: string;
    physical_location: string;
    merchant_img_url: string;
}

export interface  VerifyMerchantAgentResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
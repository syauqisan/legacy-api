interface Contact{
    contact_id_type: string;
    contact_id: string;
    deleted: boolean;
}

export interface UserContactsParam{
    country_shortname: string;
    contacts: Array<Contact>;
}

interface AlreadyFunduUsers{
    contact_id: string;
}

interface InvitedContact{
    contact_id: string;
    ii: string;
}

export interface UserContactsResponse{
    alreadyAdded: Array<AlreadyFunduUsers>;
    alreadyInvited: Array<InvitedContact>;
    successfullyInvited: Array<InvitedContact>;
    message: string;
}

interface Contact{
    contact_id_type: string;
    contact_id: string;
    deleted: boolean;
}

export interface SaveContactToServerParam{
    contacts: Array<Contact>;
    device_id: string;
    VERSION_CODE: string;
}

/**
 * in.co.eko.fundu.models.ContactResponseItem
 */
interface ContactResponseItem{
    dummy_customer: boolean;
    customer_id: string;
    contact_id_type: string;
    contact_id: string;
    contact: string;
    active: boolean;
    dummy: boolean;
    device_id: string;
    deleted: boolean;
    autocashout: boolean;
    ii: string;
    rating: number;
    i_incentive: number;
    list_specific_id: number;
}

export interface SaveContactToServerResponse{
    contact_response_list: Array<ContactResponseItem>;
}
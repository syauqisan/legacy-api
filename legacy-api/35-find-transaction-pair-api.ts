interface Coordinates{
    coordinates: Array<string>;
}

interface SenderObject{
    alias: string;
    id_type: string;
    id: string;
}

interface RecepientObject{
    alias: string;
    id_type: string;
    id: string;
}

export interface FindTransactionPairRequestParam{
    location: Coordinates;
    currency: string;
    fee: string;
    amount: number;
    hold_timeout: number;
    sender: SenderObject;
    recepient: RecepientObject;
}
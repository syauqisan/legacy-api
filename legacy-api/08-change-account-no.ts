export interface ChangeAccountNoParam {
    custid: string;
    country_shortname: string;
    accno: string; 
    fundu_pin: string;
    /**
     * this when message from gcm = Change your account number
     */
    seeker_custid?: string;
}


export interface  ChangeAccountNoResponse {
    status: 'ERROR' | 'Success';
    message?: string;
}
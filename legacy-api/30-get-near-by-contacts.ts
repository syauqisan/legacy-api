/**
 * in.co.eko.fundu.models.ContactsNearByModel
 */
interface ContactNearByModel{
    distance: number;
    distanceInTime: number;
    customer_id: string;
}

export interface GetNearByContactsParam{
    customer_neighbor_response_list: Array<ContactNearByModel>;
}
export interface VerifyOtpApiParam{
    otp: string;
    country_shortname: string;
}

interface InnerObject{
    verified: boolean;
    custid: string;
    country_shortname: string;
    contact_type: string;
    business_name: string;
    opening_time: string;
    closing_time: string;
    days: string;
    merchant_img_url: string;
    allow_withdraw: string;
    vpa: string;
}

export interface VerifyOtpApiResponse{
    status: string;
    message: string;
    data: InnerObject;
    fundu_auth_token: string;
}
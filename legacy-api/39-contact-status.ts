interface InvitedContact{
    contact_id: string;
    ii: string;
}

export interface ContactStatusResponse{
    data: Array<InvitedContact> // no matter the name, source code just use index
}
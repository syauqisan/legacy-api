interface UpdateRatingParam {
    rated_by_id: string; 
    transaction_id: string; 
    rating: number;
}

interface UpdateRatingResponse {
    transaction_id: string;
    transaction_state: number;
}